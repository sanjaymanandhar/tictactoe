require_relative "board.rb"
require_relative "constants.rb"
require "byebug"

class TicTacToe

    def initialize
        @board = Board.new
        @board.display_board_value
        @result = {
            X: [],
            O: []
        }

    end


    def input_into_index(user_input)
        @user_choice = user_input.to_i - 1
    end
    
    def current_player
        turn_count%2 == 0 ? player = "X" : player = "O"
    end

    def move(input_into_index,player)
        @board.board_value[input_into_index] = player
        if player == "X"
            @result[:X].push(input_into_index)
        else
            @result[:O].push(input_into_index)
        end
    end

    def position_taken?(input_into_index)
        if @board.board_value[input_into_index] == "X" || @board.board_value[input_into_index] == "O"
            true
        else
            false
        end
    end

    def valid_move?(input_into_index)
        # !(@board.board_value.position_taken?(input_into_index)) && input_into_index.between?(0,8)

        !position_taken?(input_into_index) && @board.board_value[input_into_index]

    end

    def turn_count
        count = 0
        @board.board_value.each do |i|
            if i == "X" || i == "O"
                count += 1
            end
        end
        return count
    end


    def turn    
        puts "Enter your choice 1-9 > "
        choice = gets.chomp
        position = input_into_index(choice)
        if valid_move?(position)
            move(position, current_player)
            @board.display_board_value
        else
            turn
        end
    end

    def won?
        WIN_COMBINATIONS.detect do |combo|
            combo == @result[:X] or combo==@result[:O]
        end
    end
    
    def full?
        turn_count == 9
    end

    def draw?
        !won? && full?
    end

    def over?
        won? || full? || draw?
    end

    def winner
        #winner method returns the array combo of the winner's pattern
        won = ""
        if winner = won?
            won = @board.board_value[winner.first]
        end
    end

    def play
        until over?
            turn
        end

        if won?
            winner = winner()
            puts "Congratulations #{winner}!"
                # play_again
        elsif draw?
            puts "Its a DRAW"
            # play_again
        end
    end

    
end

