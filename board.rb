require "byebug"
class Board
    
    def initialize
        @board_value = Array.new(9," ")
    end

    attr_accessor :board_value 


    def display_board_value
        puts "#{@board_value[0]} | #{@board_value[1]} | #{@board_value[2]} "
        puts "--+---+---"
        puts "#{@board_value[3]} | #{@board_value[4]} | #{@board_value[5]} "
        puts "--+---+---"
        puts "#{@board_value[6]} | #{@board_value[7]} | #{@board_value[8]} "
        puts "--+---+---"
    end  

    # def position_taken?(input_into_index)
    #     if @board[input_into_index] == "X" || @board[input_into_index] == "O"
    #         true
    #     else
    #         false
    #     end
    # end
end