Documentation
=============

sanjay
=============

Read the Docs
-------------

Some of Smarter Sorting's repositories have Read the Docs documentation generated with Sphinx.

* `Read the Docs Basics <https://readthedocs.org/>`_
* `Sphinx Basics <https://www.sphinx-doc.org/en/master/index.html>`_
* `Smarter Sorting Read the Docs Dashboard <https://readthedocs.com/dashboard/>`_
* `Portal API Read the Docs <https://smarter-sorting-docs.readthedocs-hosted.com/projects/smarter-sorting-compliance-platform-api/en/latest/>`_.

Endpoint Documentation
----------------------


* sanjay 
	- c
		+ h
		   `read the docs <https://readthedocs.org/>`_

* manandhr
* address banepa

1. age
2. Home
3. work

Contributing to the Documentation
---------------------------------

* Install Python.
* Install Sphinx (``pip install sphinx-autobuild``). Verify the installation (``sphinx-build --version``).
* Change directory to the /docs folder in the project.
* Modify the .rst files. Use the command ``make html`` to locally rebuild the html files (found in docs/_build/html).
* Open the html files in the browser to preview.

.. note::
  .rst files are reStructuredText files. reStructuredText has some similarities to markdown. Read more about
  reST `here <https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html>`_.
