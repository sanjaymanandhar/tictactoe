Connecting to Other Services
============================

Smarter Sorting Services
------------------------

Portal UI
^^^^^^^^^

The `Portal UI <https://github.com/Smarter-Sorting/compliance-platform-ui>`_ repo has README documentation for
getting started. Be sure to check out the Auth0 section below for authentication help.


Binning and Factory APIs
^^^^^^^^^^^^^^^^^^^^^^^^
The binning service and factory service endpoints can be hit directly using the following steps:

* Ensure kubectl is pointed to the correct cluster (staging or production).

.. code-block:: console

  $ kubectl config current-context
  $ kubectl config get-contexts
  $ kubectl config use-context <context>

* Port forward your local traffic.

.. code-block:: console

  $ kubectl port-forward -n factory service/factory-api 5000
  $ kubectl port-forward -n binning service/api-binning 5000

* Call the endpoints using postman or curl.

.. code-block::

  http://127.0.0.1:5000/flow/process_ingredients
  http://127.0.0.1:5000/product/{product_upc}/bins

To hit these service endpoints through the portal API using postman or curl, or through the portal UI, set the
``INTERNAL_FACTORY_DOMAIN`` or ``INTERNAL_BINNING_DOMAIN`` environment variables to ``127.0.0.1:5000`` in the env.dev file.
Do not commit these changes.

External Services
-----------------

Auth0
^^^^^

Auth0 Homepage: `Smarter Sorting Auth0 <https://manage.auth0.com/dashboard/us/smartersorting-production/>`_

Because Auth0 is not configured to authenticate a local development environment, authentication is accomplished
using the staging environment even in the local environment. The local user to be authenticated must also exist in the
staging database (at least an identical email), and the ``auth0-connection-name`` field of the local user should have the
value ``portal-staging-db``.

Stripe
^^^^^^

Stripe Homepage: `Smarter Sorting Stripe <https://dashboard.stripe.com/dashboard>`_

Billing Implementation Documentation: `Billing History <https://drive.google.com/drive/folders/1ci_JyQojkZGzAo3HdiQwlfNeajIA5nPa>`_

To hit Stripe endpoints in a rails console, set ``STRIPE_API_KEY`` with the value of the test key found in
staging.sops.env.


Note
---------------------

.. note::
   Environment variables for local development are located in the .env.test and .env.dev files. Environment
   variables for cloud environments are located in a .env folder. In order to secure sensitive information,
   some of these variables are in encrypted sops files.

Caution
-----------------

.. caution::
   Environment variables for local development are located in the .env.test and .env.dev files. Environment
   variables for cloud environments are located in a .env folder. In order to secure sensitive information,
   some of these variables are in encrypted sops files.

Hint
-----------------

.. hint::
   Environment variables for local development are located in the .env.test and .env.dev files. Environment
   variables for cloud environments are located in a .env folder. In order to secure sensitive information,
   some of these variables are in encrypted sops files.

Code Block
-----------------
.. code-block::

  sudo apt-get install update