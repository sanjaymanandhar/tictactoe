.. test documentation master file, created by
   sphinx-quickstart on Wed Nov 17 12:26:30 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to compliance-platform-api's documentation!
===================================================

The `Portal API <https://github.com/Smarter-Sorting/compliance-platform-api>`_ is a Ruby on Rails application that is the back-end
counterpart to the `Portal UI <https://github.com/Smarter-Sorting/compliance-platform-ui>`_. It utilizes a PostgreSQL database and interfaces
with external resources like Stripe and TaxJar and internal services like `api-factory <https://github.com/Smarter-Sorting/api-factory>`_ and
`api-binning <https://github.com/Smarter-Sorting/api-binning>`_.

.. note::
   This documentation is currently being generated.

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   Getting Started <getting_started>
   New convention <new>
   connection <connections>
 



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
