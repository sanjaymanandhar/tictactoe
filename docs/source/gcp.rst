Google Cloud Overview
=====================

GCP Resources
-------------

* `Developing with Google Cloud <https://docs.google.com/document/d/1fRbyGh76X8HNllRyOaKt__9MBiu6ROqX3EAI-QdwPvI/edit#>`_
* `Dashboard for GCP Staging Builds <https://console.cloud.google.com/cloud-build/builds?project=ss-platform-staging>`_
* `Dashboard for GCP Production Builds <https://console.cloud.google.com/cloud-build/builds?project=ssftp-220720>`_

Environment Variables
---------------------
:: caution:
   Environment variables for local development are located in the .env.test and .env.dev files. Environment
   variables for cloud environments are located in a .env folder. In order to secure sensitive information,
   some of these variables are in encrypted sops files.