require_relative "board.rb"
require_relative "tictactoe.rb"



def play_game
    game = TicTacToe.new()
    game.play
    play_again
end

def play_again
    puts "Play again[y/n]: "
    input_from_user = gets.chomp.downcase
    if input_from_user =="y"
        play_game
    else
        puts "have a good day"
    end
end
play_game